# README #

This VQMod adds a script to registration form to automagically load city name when postal number is entered. Uses API from Norwegian Posten/Bring. 

### What is this repository for? ###

It is to help secure a correct postal number and city in customers postal address.

### How do I get set up? ###

Made for OC2.

Just extract archive and upload to you server. No files are replaced.
You should ALWAYS back up your data before modifying it.

### Contribution guidelines ###

Feel free to change whatever you want.

### Who do I talk to? ###

Please test and give me some feedback if something does not work. Since we give this away for free we do not give you any guaranties for functionality. We also do NOT make customized versions of this mod. Hire a developer... 
